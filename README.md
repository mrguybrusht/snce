# S'nce Group developer exercise
 
## Services exposed

| Service | Port | Notes |
| --- | --- | --- |
| Apache | 8080 | --- |
| MySQL | 3306 | --- |

  * Apache to port 8080
  * MySQL to port 3306

## How to run

1. `docker-compose up --build -d`
2. `docker exec php composer install -d /var/www/html/snce`
3. Enjoy :)