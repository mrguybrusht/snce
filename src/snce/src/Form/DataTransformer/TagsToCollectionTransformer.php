<?php

namespace App\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

class TagsToCollectionTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    public function transform($tags)
    {
        return $tags;
    }

    public function reverseTransform($tags)
    {
        $tagCollection = new ArrayCollection();

        $tagsRepository = $this->manager
            ->getRepository('App:Tag');

        foreach ($tags as $tag) {
            $tagInRepo = $tagsRepository->findOneByName($tag->getName());

            if ($tagInRepo !== null) {
                $tagCollection->add($tagInRepo);
            } else {
                $tagCollection->add($tag);
            }
        }

        return $tagCollection;
    }
}
