<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename     = preg_replace('/[^A-Za-z0-9_]/', '-', strtolower($originalFilename));
        $fileName         = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
        
        $resizedImage = $this->resizeImage($file->getPathname(), 200, 200, true);
        imagejpeg($resizedImage, $this->getTargetDirectory() . '/' . $fileName);
        
        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    public function resizeImage($file, $w, $h, $crop = false)
    {
        list($width, $height) = getimagesize($file);
        $r                    = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth  = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth  = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth  = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }
}
