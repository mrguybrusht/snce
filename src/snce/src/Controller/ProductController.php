<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Tag;
use App\Form\ProductType;
use App\Service\ImageUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/product/create", name="product_create")
     */
    public function create(Request $request, ImageUploader $imageUploader)
    {
        $product = new Product();
        $product->getTags()->add((new Tag()));

        $form = $this->createForm(ProductType::class, $product)
                     ->add('create', SubmitType::class, ['label' => 'Create']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();

            $imageFile = $form['image']->getData();
            if ($imageFile) {
                $imageFileName = $imageUploader->upload($imageFile);
                $product->setImage($imageFileName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product_list');
        }

        return $this->render('product/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/product/{productId}/edit", name="product_edit")
     */
    public function edit(Request $request, ImageUploader $imageUploader, $productId)
    {
        $product = $this->getDoctrine()
                        ->getRepository(Product::class)
                        ->find($productId);

        $form = $this->createForm(ProductType::class, $product)
                     ->add('save', SubmitType::class, ['label' => 'Save']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentProductImage = $product->getImage();
            $product             = $form->getData();

            $imageFile = $form['image']->getData();
            if ($imageFile) {
                $imageFileName = $imageUploader->upload($imageFile);
                $product->setImage($imageFileName);
                if ($currentProductImage) {
                    unlink($this->getParameter('images_directory') . '/' . $currentProductImage);
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product_list');
        }

        return $this->render('product/edit.html.twig', [
            'form'    => $form->createView(),
            'product' => $product
        ]);
    }

    /**
     * @Route("/product/list", name="product_list")
     */
    function list(Request $request) {

        $products = $this->getDoctrine()
                         ->getRepository(Product::class)
                         ->findBy([], ['created' => 'DESC']);

        $tags = array_reduce($this->getDoctrine()
                                      ->getRepository(Tag::class)
                                      ->findAll(), function ($carry, $item) {
                $carry[$item->getName()] = $item->getId();
                return $carry;
            }, []);

        $form = $this->createFormBuilder(['message' => 'Choose filter'])
                     ->add('tag', ChoiceType::class, ['label' => false, 'choices' => $tags])
                     ->add('filter', SubmitType::class, ['label' => 'Filter'])
                     ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $tagId = $form['tag']->getData();
            if (in_array($tagId, $tags)) {
                return $this->redirectToRoute('product_filtered_list', ['tagId' => $tagId], 302);
            }
        }

        return $this->render('product/list.html.twig', [
            'form'     => $form->createView(),
            'products' => $products,
            'tags'     => $tags
        ]);
    }

    /**
     * @Route("/product/list/tag/{tagId}", name="product_filtered_list")
     */
    public function filteredList($tagId)
    {
        $products = [];
        $tag      = $this->getDoctrine()
                         ->getRepository(Tag::class)
                         ->find($tagId);

        if ($tag) {
            $products = $tag->getProducts();
        }

        return $this->render('product/filtered-list.html.twig', [
            'products' => $products,
            'tag'      => $tag->getName()
        ]);
    }
}
