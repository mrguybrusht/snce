jQuery(document).ready(function() {
    bsCustomFileInput.init()
    
    var $collectionHolder = $('ul.tags');
    $collectionHolder.data('index', $collectionHolder.find('.list-group-item').length);
    chechRemoveButtonStatus($collectionHolder);
    
    $('#add-tag-link').on('click', function(e) {
        e.preventDefault();
        $collectionHolder.append(addTagForm($collectionHolder));
        chechRemoveButtonStatus($collectionHolder);
    });

    $(document).on('click', '.remove-tag', function(e) {
        e.preventDefault();
        if ($collectionHolder.find('.list-group-item').length > 1) {
            $(this).closest('.list-group-item').remove();
        }
        chechRemoveButtonStatus($collectionHolder);
        return false;
    });
});

function addTagForm($collectionHolder) {
    var prototype = $collectionHolder.data('prototype');
    var index = $collectionHolder.data('index');
    var newForm = prototype.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    return newForm;
}

function chechRemoveButtonStatus($collectionHolder) {
    $('.remove-tag').attr('disabled', ($collectionHolder.find('.list-group-item').length == 1));
}